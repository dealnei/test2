package es2;
import java.util.Arrays;
import java.util.Random;
public class Test {

	public static void main(String[] args) {
		Random rnd = new Random();
		int[] arr=new int[10];
		arr=rnd.ints(10,0,10).toArray();
		
		System.out.println(Arrays.toString(arr));
		// TODO Auto-generated method stub
		int pari=0,dispari=0;
		for(int i=0;i<arr.length;i++) {
			if(i%2==0) {
				pari=pari+arr[i];
			}
			else {
				dispari=dispari+arr[i];
			}
		}
		if(pari==dispari) {
			System.out.println("pari uguale a dispari");
		}
		else {
			System.out.println("pari diverso da dispari");
		}
	}

}
